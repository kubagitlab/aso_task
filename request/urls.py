from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from request import views

urlpatterns = [
    path('performers/', views.PerformerList.as_view()),
    path('requests/', views.RequestList.as_view()),
    path('calculations/', views.CalculationList.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)

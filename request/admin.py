from django.contrib import admin
from .models import *

admin.site.register(Performer)
admin.site.register(Position)
admin.site.register(Request)
admin.site.register(Status)
admin.site.register(Calculation)

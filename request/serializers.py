from rest_framework import serializers
from .models import *


class PerformerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Performer
        fields = '__all__'


class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = '__all__'


class PositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Position
        fields = ['id', 'name']


class RequestSerializer(serializers.ModelSerializer):
    status = StatusSerializer(many=False, read_only=True)
    position = PositionSerializer(many=False, read_only=True)

    class Meta:
        model = Request
        fields = '__all__'


class CalculationSerializer(serializers.ModelSerializer):
    request = RequestSerializer(many=False, read_only=True)

    class Meta:
        model = Calculation
        fields = '__all__'


class RequestHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = RequestHistory
        fields = '__all__'

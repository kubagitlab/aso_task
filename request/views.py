from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from django.conf import settings
from request.serializers import *


class PerformerList(APIView):
    """
    List all Performers, or create a new Performer.
    """

    def get(self, request):
        performers = Performer.objects.all()
        serializer = PerformerSerializer(performers, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = PerformerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RequestList(APIView):
    """
    List all Request, or create a new Request.
    """

    def get(self, request):
        requests = Request.objects.all()
        # print('qqq:', str(requests.query))

        if request.data.get('status', ''):
            requests = requests.filter(status=request.data.get('status'))

        serializer = RequestSerializer(requests, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = RequestSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            history_data = {'request': serializer.data['id'], 'status': settings.STATUS_CREATE}
            his_serializer = RequestHistorySerializer(data=history_data)
            if his_serializer.is_valid():
                his_serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CalculationList(APIView):
    def get(self, request):
        data = Calculation.objects.all()

        if request.data.get('request', ''):
            data = data.filter(request=request.data['request'])
        if request.data.get('performer', ''):
            data = data.filter(performer=request.data['performer'])

        serializer = CalculationSerializer(data, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = CalculationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            history_data = {'request': serializer.data['request'], 'status': settings.STATUS_APPLIED,
                            'performer': serializer.data['performer']}
            his_serializer = RequestHistorySerializer(data=history_data)
            if his_serializer.is_valid():
                his_serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class HistoryList(APIView):
    def get(self, request):
        data = RequestHistory.objects.all()
        serializer = RequestHistorySerializer(data, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = RequestHistorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

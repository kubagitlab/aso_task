from django.db import models
from django.utils import timezone


class Performer(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Status(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Position(models.Model):
    name = models.CharField(max_length=50)
    code_okpd2 = models.CharField(max_length=50)
    code_okei = models.IntegerField()
    amount = models.IntegerField()

    def __str__(self):
        return self.name


class Request(models.Model):
    name = models.CharField(max_length=50)
    position = models.ForeignKey(Position, on_delete=models.CASCADE)
    status = models.ForeignKey(Status, on_delete=models.CASCADE, default=1)
    created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name


class Calculation(models.Model):
    performer = models.ForeignKey(Performer, on_delete=models.CASCADE)
    request = models.ForeignKey(Request, on_delete=models.CASCADE)
    price = models.FloatField()
    created = models.DateTimeField(default=timezone.now)


class RequestHistory(models.Model):
    request = models.ForeignKey(Request, on_delete=models.CASCADE)
    performer = models.ForeignKey(Performer, on_delete=models.CASCADE, null=True)
    status = models.ForeignKey(Status, on_delete=models.CASCADE, default=1)
    created = models.DateTimeField(default=timezone.now)
